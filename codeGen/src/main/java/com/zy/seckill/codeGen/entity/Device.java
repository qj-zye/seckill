package com.zy.seckill.codeGen.entity;

import io.swagger.annotations.ApiModelProperty;
import javax.annotation.Generated;

public class Device {
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Long id;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String name;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Long type;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String deviceSerial;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String deviceIp;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Byte deviceStatus;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Byte direction;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String location;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Double longitude;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Double latitude;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Integer orderNum;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Byte status;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String plotId;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String playUrl;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String gbdeviceCode;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String gbchannelCode;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String extData;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Long createTime;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Long updateTime;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Long getId() {
        return id;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setId(Long id) {
        this.id = id;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getName() {
        return name;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Long getType() {
        return type;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setType(Long type) {
        this.type = type;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getDeviceSerial() {
        return deviceSerial;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setDeviceSerial(String deviceSerial) {
        this.deviceSerial = deviceSerial == null ? null : deviceSerial.trim();
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getDeviceIp() {
        return deviceIp;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setDeviceIp(String deviceIp) {
        this.deviceIp = deviceIp == null ? null : deviceIp.trim();
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Byte getDeviceStatus() {
        return deviceStatus;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setDeviceStatus(Byte deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Byte getDirection() {
        return direction;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setDirection(Byte direction) {
        this.direction = direction;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getLocation() {
        return location;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setLocation(String location) {
        this.location = location == null ? null : location.trim();
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Double getLongitude() {
        return longitude;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Double getLatitude() {
        return latitude;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Integer getOrderNum() {
        return orderNum;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Byte getStatus() {
        return status;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setStatus(Byte status) {
        this.status = status;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getPlotId() {
        return plotId;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setPlotId(String plotId) {
        this.plotId = plotId == null ? null : plotId.trim();
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getPlayUrl() {
        return playUrl;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setPlayUrl(String playUrl) {
        this.playUrl = playUrl == null ? null : playUrl.trim();
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getGbdeviceCode() {
        return gbdeviceCode;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setGbdeviceCode(String gbdeviceCode) {
        this.gbdeviceCode = gbdeviceCode == null ? null : gbdeviceCode.trim();
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getGbchannelCode() {
        return gbchannelCode;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setGbchannelCode(String gbchannelCode) {
        this.gbchannelCode = gbchannelCode == null ? null : gbchannelCode.trim();
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getExtData() {
        return extData;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setExtData(String extData) {
        this.extData = extData == null ? null : extData.trim();
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Long getCreateTime() {
        return createTime;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Long getUpdateTime() {
        return updateTime;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }
}