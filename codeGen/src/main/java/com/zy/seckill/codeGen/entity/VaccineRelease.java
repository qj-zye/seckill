package com.zy.seckill.codeGen.entity;

import io.swagger.annotations.ApiModelProperty;
import javax.annotation.Generated;

public class VaccineRelease {
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Long id;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String name;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Long siteId;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String siteName;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Integer dose;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Integer amount;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Integer dockAmount;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Long useupTime;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Integer version;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Long startTime;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Long vaccineId;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String vaccineName;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String vaccineBatch;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String manufacturer;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Long doseTime;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Long timePeriod;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String timeperiodName;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String contactName;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private String contactMobile;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Byte status;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    private Long createTime;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Long getId() {
        return id;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setId(Long id) {
        this.id = id;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getName() {
        return name;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Long getSiteId() {
        return siteId;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getSiteName() {
        return siteName;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setSiteName(String siteName) {
        this.siteName = siteName == null ? null : siteName.trim();
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Integer getDose() {
        return dose;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setDose(Integer dose) {
        this.dose = dose;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Integer getAmount() {
        return amount;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Integer getDockAmount() {
        return dockAmount;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setDockAmount(Integer dockAmount) {
        this.dockAmount = dockAmount;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Long getUseupTime() {
        return useupTime;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setUseupTime(Long useupTime) {
        this.useupTime = useupTime;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Integer getVersion() {
        return version;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setVersion(Integer version) {
        this.version = version;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Long getStartTime() {
        return startTime;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Long getVaccineId() {
        return vaccineId;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setVaccineId(Long vaccineId) {
        this.vaccineId = vaccineId;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getVaccineName() {
        return vaccineName;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setVaccineName(String vaccineName) {
        this.vaccineName = vaccineName == null ? null : vaccineName.trim();
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getVaccineBatch() {
        return vaccineBatch;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setVaccineBatch(String vaccineBatch) {
        this.vaccineBatch = vaccineBatch == null ? null : vaccineBatch.trim();
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getManufacturer() {
        return manufacturer;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer == null ? null : manufacturer.trim();
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Long getDoseTime() {
        return doseTime;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setDoseTime(Long doseTime) {
        this.doseTime = doseTime;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Long getTimePeriod() {
        return timePeriod;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setTimePeriod(Long timePeriod) {
        this.timePeriod = timePeriod;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getTimeperiodName() {
        return timeperiodName;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setTimeperiodName(String timeperiodName) {
        this.timeperiodName = timeperiodName == null ? null : timeperiodName.trim();
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getContactName() {
        return contactName;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setContactName(String contactName) {
        this.contactName = contactName == null ? null : contactName.trim();
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String getContactMobile() {
        return contactMobile;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setContactMobile(String contactMobile) {
        this.contactMobile = contactMobile == null ? null : contactMobile.trim();
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Byte getStatus() {
        return status;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setStatus(Byte status) {
        this.status = status;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public Long getCreateTime() {
        return createTime;
    }

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }
}